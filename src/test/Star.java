package test;

import java.util.Collections;

public class Star {

	public static void main(String args[]) {

		String s1 = "My name in a box by Sachin";
		String[] s2List = s1.split(" ");
		Integer countStarsCol = s1.length();
		String stars = String.join("", Collections.nCopies(countStarsCol, "*"));
		System.out.println(stars);

		for (String strObj : s2List) {

			Integer currStr = strObj.length() + 3;
			Integer finalSkipCount = countStarsCol - currStr;
			String blankSpace = String.join("", Collections.nCopies(finalSkipCount, " "));
			System.out.println("* "+strObj+blankSpace+"*");
		}

		System.out.println(stars);
	}
}
